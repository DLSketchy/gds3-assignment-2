﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelS: MonoBehaviour
{

    public GameObject CurrentLevel;
    public GameObject NextLevel;
    public GameObject CurrentLevelEnd;
    public GameObject NextLevelEnd;
    public GameObject Player;

    private Vector3 originalPos;

    // Use this for initialization
    void Start()
    {
        originalPos = Player.transform.position;
    }

    // Update is called once per frame
    void OnCollisionEnter(Collision col)
    {

        CurrentLevel.SetActive(false);
        NextLevel.SetActive(true);
        CurrentLevelEnd.SetActive(false);
        NextLevelEnd.SetActive(true);
        Player.transform.position = originalPos;
        col.rigidbody.velocity = Vector3.zero;
        Debug.Log("fuck");

    }
}
