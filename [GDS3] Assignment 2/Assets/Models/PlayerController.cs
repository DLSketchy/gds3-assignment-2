﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    /* {
        pub
        Transform thisTrans;

        [SerializeField]
        Transform camera;

        // Use this for initialization
        void Start()
        {
            thisTrans = this. ;
        }

        // Update is called once per frame
        void Update()
        {

            thisTrans.position += camera.forward * Input.GetAxis("Vertical");
        }
    } */

    public float rotateSpeed;

    private Vector3 rotateDirection;

    void FixedUpdate()
    {
        rotateDirection = new Vector3(Input.GetAxis("Vertical"), 0, -Input.GetAxis("Horizontal"));

        rotateDirection *= rotateSpeed;

        rotateDirection = Camera.main.transform.rotation * rotateDirection;

        transform.Rotate(rotateDirection);

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity, 0.015f);
    }


}