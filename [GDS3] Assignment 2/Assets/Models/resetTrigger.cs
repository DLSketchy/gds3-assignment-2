﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class resetTrigger : MonoBehaviour {

    public Vector3 originalPos;

    private void Start()
    {
        originalPos = gameObject.transform.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        gameObject.transform.position = originalPos;
    }

}
